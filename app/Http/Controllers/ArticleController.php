<?php

namespace App\Http\Controllers;

use App\Article;
use App\Http\Requests\ArticleRequest;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Console\Input\Input;
use Tymon\JWTAuth\Contracts\Providers\Auth;
use JWTAuth;

class ArticleController extends Controller
{
    public function image(Request $request)
    {
        $request->validate([
        'image' => 'required|mimes:jpg,png,jpeg',
         ]);

        $fileContent = file_get_contents($request->file('image'));
        $extention = $request->file('image')->getClientOriginalExtension();
        $fileName = date('mdYHis') . '_' . uniqid() . '.' . $extention;
        $filePath = 'public/images/';

        Storage::disk('local')->put($filePath . $fileName, $fileContent);

        return Storage::url("images/{$fileName}");
    }
    public function index()
    {
        $articles = Article::all();
        return response()->json($articles );
    }
    public function upload(Request $request)
    {
        try {
            $request->validate([
                'file' => 'required|mimes:jpg,png,jpeg',
            ]);
        } catch (Exception $e) {
            report($e);
//            return response()->json([
//                'status' => 'false',
//                'message' => 'sccsacas'
//            ], 422);
            return false;
        }

        $fileContent = file_get_contents($request->file('file'));
        $extention = $request->file('file')->getClientOriginalExtension();
        $fileName = date('mdYHis') . '_' . uniqid() . '.' . $extention;
        $filePath = 'public/images/';

        Storage::disk('local')->put($filePath . $fileName, $fileContent);

        return Storage::url("images/{$fileName}");


    }

    public function store(ArticleRequest $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
//        $article = Article::find(1);
        // Create a new article
        $article = Article::create([
            'body' => $request->get('body') ,
            'title' => $request->get('title') ,
            'user_id' =>$user->id  ,
            'category_id' =>$request->get('category_id'),
            'photo_path'=>$request->get('photo_path')
        ]);


        $article->save();
        $article->tags()->sync($request->get('tags'));

        return response()->json($article );


    }

    public function show(Request $request, Article $article)
    {
        return response()->json(compact('article'));
    }
}
