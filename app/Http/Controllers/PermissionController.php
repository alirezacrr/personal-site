<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Contracts\Providers\Auth;

class PermissionController extends Controller
{
    public function store(Request $request)
    {

        $all_per = $request->input('permission');
        foreach ($all_per as $per){
            Permission::create([
                'name' => $per
            ]);
        }
//        $role = Role::create(['name' => 'test2']);
//        $permission = Permission::create(['name' => );
//        $permission->assignRole($role);
        return response()->json([
            'status' => 'success',
            'message' => $all_per
        ], 200);
    }
    function check($permissionName) {
        if (!auth()->user()->hasPermissionTo($permissionName)) {
            abort(403);
        }
        return response('', 204);
    }
}
