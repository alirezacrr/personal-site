<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleController extends Controller
{
    public function index()
    {
       $roles =  Role::all();
        return response()->json($roles);
    }
    public function store(Request $request)
    {
        $role = Role::create([
            'name' => $request->input('role')
        ]);
        $permission =$request->input('permission');
        $role->givePermissionTo($permission);
    }
    public function assign(Request $request)
    {
        $role =  Role::find(14);
//        $role = $request->input('role');
        $permission =$request->input('permission');
       return $role->givePermissionTo($permission);
    }

}
