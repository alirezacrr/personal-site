<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Photo extends Model
{
    protected $fillable = ['path' , 'name' , 'thumbnail_path'];
    protected $file;


    public function baseDir()
    {
        return 'posts_img/photos' ;
    }
    public function post()
    {
        return $this->belongsTo(Post::class);
    }
    public static function File(UploadedFile $file)
    {
        $photo = new static;
        $photo->file = $file;
        $photo->fill([
            'name' => $photo->fileName(),
            'path' => $photo->filePath(),
            'thumbnail_path' => $photo->thumbnailPath(),
        ]);
        return $photo;
    }

    public function fileName()
    {
        $name = sha1(
            $this->file->getClientOriginalName()
        );
        $extension = $this->file->getClientOriginalExtension();
        return "{$name}.{$extension}";
    }
    public function filePath()
    {
        return $this->baseDir() . '/' . $this->fileName();
    }
    public function thumbnailPath()
    {
        return $this->baseDir() . '/tn-' . $this->fileName();

    }
    public function upload()
    {
        $this->file->move($this->baseDir() , $this->fileName());
        $this->makeThumbnail();

        return $this;
    }
    public function makeThumbnail()
    {
        Image::make($this->filePath())
            ->fit(200)
            ->save($this->thumbnailPath());
        return $this;
    }

}
