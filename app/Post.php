<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Post extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable = [
        'body',
    ];
//    public function category()
//    {
//        return $this->belongsTo(Category::class);
//    }
//    public function tags()
//    {
//        return $this->belongsToMany(Tag::class);
//    }
//    public function user()
//    {
//        return $this->belongsTo(User::class);
//    }
//    public function photos()
//    {
//        return $this->hasMany(Photo::class);
//    }
//    public function addPhoto(Photo $photo)
//    {
//        return $this->photos()->save($photo);
//    }
}
