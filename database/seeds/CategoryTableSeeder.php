<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create(['name' => 'تکنولوژی']);
        Category::create(['name' => 'فیلم و سریال']);
        Category::create(['name' => 'اموزشی']);
        Category::create(['name' => 'دل نوشته']);
        Category::create(['name' => 'شعر']);

    }
}
