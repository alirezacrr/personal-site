<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'edit articles']);
        Permission::create(['name' => 'delete articles']);
        Permission::create(['name' => 'publish articles']);
        Permission::create(['name' => 'unpublish articles']);
        Permission::create(['name' => 'add roles']);
        Permission::create(['name' => 'add permission']);
        Permission::create(['name' => 'edit users']);
        Permission::create(['name' => 'content use']);

        // gets all permissions via Gate::before rule; see AuthServiceProvider

        $role1 = Role::create(['name' => 'admin']);
        $role1->givePermissionTo('edit articles');
        $role1->givePermissionTo('delete articles');
        $role1->givePermissionTo('publish articles');
        $role1->givePermissionTo('unpublish articles');
        $role1->givePermissionTo('add roles');
        $role1->givePermissionTo('add permission');
        $role1->givePermissionTo('edit users');
        $role1->givePermissionTo('content use');;

        // create roles and assign existing permissions

        $role2 = Role::create(['name' => 'manager']);
        $role2->givePermissionTo('edit articles');
        $role2->givePermissionTo('delete articles');
        $role2->givePermissionTo('publish articles');
        $role2->givePermissionTo('unpublish articles');
        $role2->givePermissionTo('content use');;

        $role3 = Role::create(['name' => 'author']);
        $role3->givePermissionTo('publish articles');
        $role3->givePermissionTo('unpublish articles');
        $role3->givePermissionTo('content use');;

        $role4 = Role::create(['name' => 'user']);
        $role4->givePermissionTo('content use');


        // create demo users
        $user = Factory(App\User::class)->create([
            'name' => 'Example User',
            'email' => 'user@example.com',
            // factory default password is 'secret'
        ]);
        $user->assignRole($role4);

        $user = Factory(App\User::class)->create([
            'name' => 'Example Admin User',
            'email' => 'admin@example.com',
            // factory default password is 'secret'
        ]);
        $user->assignRole($role1);

        $user = Factory(App\User::class)->create([
            'name' => 'Example manager User',
            'email' => 'manager@example.com',
            // factory default password is 'secret'
        ]);
        $user->assignRole($role2);
        $user = Factory(App\User::class)->create([
            'name' => 'Example author User',
            'email' => 'author@example.com',
            // factory default password is 'secret'
        ]);
        $user->assignRole($role3);


    }
}
