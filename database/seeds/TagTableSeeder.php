<?php

use App\Tag;
use Illuminate\Database\Seeder;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tag::create(['name' => 'گوشی']);
        Tag::create(['name' => 'کتاب']);
        Tag::create(['name' => 'فیلم']);
        Tag::create(['name' => 'سریال']);
        Tag::create(['name' => 'دلی']);
        Tag::create(['name' => 'علمی']);
        Tag::create(['name' => 'وب']);
        Tag::create(['name' => 'اموزش']);
    }
}
