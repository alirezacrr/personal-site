/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import Vue from 'vue'
import VueRouter from 'vue-router'
// import VueCookie from 'vue-cookie'

// Tell Vue to use the plugin

Vue.use(VueRouter);

// Vue.use(VueCookie);

import App from './views/App'
import Sidebar from './components/Sidebar'
import Home from './views/Home'
import Login from './views/Login'
import Register from './views/Register'
import Admin from './views/Admin'
import AddArticle from './views/AddArticle'
import ShowArticle from './views/ShowArticle'
import Articles from './views/Articles'



const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },{
            path: '/login',
            name: 'login',
            component: Login
        },{
            path: '/register',
            name: 'register',
            component: Register
        } ,
        {
            path: '/admin/panel/users',
            name: 'admin',
            component: Admin,
        },{
            path: '/admin/panel/add/article',
            name: 'article',
            component: AddArticle,
        },{
            path: '/show',
            name: 'show',
            component: ShowArticle,
        },{
            path: '/article',
            name: 'article',
            component: Articles,
        }
    ],
});

import { GridPlugin } from '@syncfusion/ej2-vue-grids';

Vue.use(GridPlugin);
const app = new Vue({
    el: '#app',
    components: { App ,  Sidebar},
    router,
    methods:{
        can(permissionName , callback) {
            var token = JSON.parse(localStorage.getItem('token'));
            if (token){
                axios.defaults.headers.common['Authorization'] = "Bearer " + token.value;
                console.log(token)

                // this.$root.refreshToken()
                console.log('vdsdv')
                let app = this;
                axios.post(`/api/permission/${permissionName}`).then(() => {
                    callback(true)
                    // this.myFunction()
                }).catch(() => {
                    callback(false)

                });
            }
             else {
                callback(false)
             }


        },
        checkExpire() {
            let app = this;
            var token =JSON.parse(localStorage.getItem('token'))
            var date1 = new Date();
            var date2 = new Date(token.expiry);
            var Difference_Time = date2 - date1;
            if (Difference_Time <= 0){
                // this.refreshToken()
                return false

            } else {
                return true
            }
        },
        refreshToken(callback) {
            var token =JSON.parse(localStorage.getItem('token'));
            axios.defaults.headers.common['Authorization'] =  "Bearer " + token.value;
            // var token = "YzE5ZTdiMjVlYzM5NjA2MGJkZTM5NjVlOTQ5YMmQ5ZjMwYjA0YmEzZmZjN2I1MmI4MDJkNQ";

            var headers = {
                Authorization: "Bearer " + token.value,
                Accept: "application/json, text/plain, */*",
                "Content-Type": "application/json"
            };
            if (token){
                let app = this;
                if (! this.checkExpire()){
                    axios.post("/api/refresh/token").then((response)=> {
                        const myDate = new Date();
                        let exp =response.data.expires_in;
                        myDate.setHours( myDate.getHours() + exp / (60*60) );
                        // myDate.setMinutes( myDate.getMinutes() + 1);
                        var item = {
                            value: response.data.access_token,
                            expiry: myDate
                        };
                        localStorage.setItem('token', JSON.stringify(item));
                        axios.defaults.headers.common['Authorization'] =  "Bearer " + item.value;
                        console.log(item);
                        callback(true) ;
                    }).catch((response)=> {
                        callback(false)
                    });
                } else {
                    callback(true)
                }
            } else {
                callback(false)
            }



        },
    }
});
