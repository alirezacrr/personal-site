window._ = require('lodash');

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');
window.Vue = require('vue');
import VueRouter from 'vue-router'

var VueCookie = require('vue-cookie');
// Tell Vue to use the plugin
Vue.use(VueCookie);
Vue.use(VueRouter);
// var token =JSON.parse(localStorage.getItem('token'));
// if (token != null){
//     axios.defaults.headers.common['Authorization'] =  "Bearer " + token.value;
// }
/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo';

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     forceTLS: true
// });

window.$ = window.jQuery = require('jquery');

require('./components/summernote.js');
require('bootstrap');
require('popper.js');




// function checkExpire() {
//     let app = this;
//     var token =JSON.parse(localStorage.getItem('token'))
//     var date1 = new Date();
//     var date2 = new Date(token.expiry);
//     var Difference_Time = date2 - date1;
//     if (Difference_Time <= 0){
//         console.log(false);
//         return false;
//     } else {
//         console.log(true);
//         return true
//     }
// }
// export function refreshToken() {
//     let app = this;
//
//     if (! this.checkExpire()){
//         axios.post("/api/refresh/token").then((response)=> {
//             const myDate = new Date();
//             let exp =response.data.expires_in;
//             myDate.setHours( myDate.getHours() + exp / (60*60) );
//             localStorage.setItem('token',  response.data.access_token );
//             var item = {
//                 value: response.data.access_token,
//                 expiry: myDate
//             };
//             console.log(item);
//             localStorage.setItem('token', JSON.stringify(item));
//
//         }).catch((response)=> {
//             console.log(response)
//         });
//     }
//
// }
// let token = document.head.querySelector('meta[name="csrf-token"]');
//
// if (token) {
//     window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
// } else {
//     console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
// }
//
// /
// * Echo exposes an expressive API for subscribing to channels and listening
// * for events that are broadcast by Laravel. Echo and event broadcasting
// * allows your team to easily build robust real-time web applications.
// */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: 'your-pusher-key'
// });