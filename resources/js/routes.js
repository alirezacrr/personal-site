import VueRouter from  'vue-router'

let routes = [
    {
        path : '/' ,
        component: require('./components/Sidebar')
    }
]
export default  new VueRouter({
    routes:routes
})