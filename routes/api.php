<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('register', 'UserController@register');
Route::post('login', 'UserController@authenticate');
Route::post('project', 'ProjectController@index');
Route::post('create/permission', 'PermissionController@store');
Route::post('create/role', 'RoleController@store');
Route::post('create/assign', 'RoleController@assign');
Route::post('create/permission', 'PermissionController@store');

Route::post('refresh/token', 'UserController@refresh');

Route::post('articles', 'ArticleController@index');
Route::post('image/title', 'ArticleController@image');




Route::group(['middleware' => ['jwt.verify']], function() {
    Route::post('/permission/{permissionName}', 'PermissionController@check');

    Route::get('user', 'UserController@getAuthenticatedUser');
    Route::delete('logout', 'UserController@logout');
    Route::group(['middleware' => ['role:admin']], function () {
        Route::post('upload/images/article', 'ArticleController@upload');
        Route::post('save/content/article', 'ArticleController@store');
        Route::post('get/all/tags', 'TagController@index');
        Route::post('get/all/category', 'CategoryController@index');

        Route::get('/article', 'ArticleController@create')->name('article.create');

        Route::post('/article/create', 'ArticleController@store')->name('user.store');

        Route::get('/article/{article}', 'ArticleController@show')->name('article.show');
        Route::post('/users', 'UserController@index');
        Route::post('/edit/role/{id}/{role}', 'UserController@editRole');
        Route::post('/get/roles/', 'RoleController@index');
    });
});
